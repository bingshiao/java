FROM openjdk:11 AS build

WORKDIR /src/
COPY . /src/
RUN javac Main.java -d /bin

FROM openjdk:11-jre
COPY --from=build /bin /bin
ENTRYPOINT ["java", "-cp", "/bin", "Main"]
